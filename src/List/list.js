import React from 'react';
import { Search } from '../Search/search';
import { Basic } from '../Basic/basic';
import { ViewSwitch } from '../Basic/view-switch'; 
import './list.css'

export const List = ( ) => {
    return (
        <div className='pokedex-wrapper'>
            <Basic />
            <div className='right-component'> 
                <div className='right-top'>
                    <div className='view-title'>List View</div>
                    <ViewSwitch />
                </div>
                <div className="right-container">
                    <div className="right-container-black">
                        <div className="right-container-screen">
                            <Search />
                        </div>
                    </div>
                    <div className='list-bar-wapper'>
                        <div  className='gallerry-page-bar'> Prev</div>
                        <div  className='gallerry-page-bar'> Next</div>
                    </div>
                </div>
            </div>
        </div>
    )
}
