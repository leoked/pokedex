import React from 'react';
import "./search-list.css"
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export const SearchList = ( {results,sortBy,sortOrder} ) => {
    SearchList.propTypes = {
        results: PropTypes.any,
        sortBy: PropTypes.oneOf(['Name','ID']),
        sortOrder: PropTypes.oneOf(['a','d'])
    }
    
    if(results) {
        if(sortBy === "Name" && sortOrder === 'a'){
            results.sort((a,b) => {
                if(a.name < b.name){ return -1; }
                else if(a.name > b.name){ return 1; }
                else{ return 0; }
            })
        }
        else if(sortBy === "Name" && sortOrder === 'd'){
            results.sort((a,b) => {
                if(a.name < b.name){ return 1; }
                else if(a.name > b.name){ return -1; }
                else{ return 0; }
            })
        }
        else if(sortBy === "ID" && sortOrder === 'a'){
            results.sort((a,b) => {
            if( Number(a.url.split('/')[6]) < Number(b.url.split('/')[6]) ){ return -1; }
                else if( Number(a.url.split('/')[6]) > Number(b.url.split('/')[6]) ){ return 1; }
                else{ return 0; }
            })
        }
        else if(sortBy === "ID" && sortOrder === 'd'){
            results.sort((a,b) => {
            if( Number(a.url.split('/')[6]) < Number(b.url.split('/')[6]) ){ return 1; }
                else if( Number(a.url.split('/')[6]) > Number(b.url.split('/')[6]) ){ return -1; }
                else{ return 0; }
            })
        }
        return (
            <div id = 'search-list' className='result-list result-active' >
            {
                results.map((result,id) => {
                    const urlArray = result.url.split('/')
                    const pokeId = urlArray[6]
                    return (
                        <Link key={id} to = {`/list/detail/${pokeId}`} className='search-list-element'> 
                            #{pokeId.padStart(4,'0')} {result.name.charAt(0).toUpperCase() + result.name.slice(1)} 
                        </Link>
                    )
                })
            }
            </div>
        )
    }
}