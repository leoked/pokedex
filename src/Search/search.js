import React, {useState} from 'react'
import { SearchBar } from './search-bar';
import { SearchList } from './search-list';
import "./search.css"


export const Search = () => {
    const [results, setResults] = useState([]);
    const [sortBy, setSortBy] = useState('ID');
    const [sortOrder, setSortOrder] = useState('a');
    return (
        <div className = 'search-bar-container'>  
            <SearchBar  setResults = {setResults} setSortBy={setSortBy} setSortOrder={setSortOrder}/>
            <SearchList  results = {results} sortBy={sortBy} sortOrder={sortOrder}/>
        </div>
    );
}

