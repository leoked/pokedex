import React, { useState } from 'react'
import "./search-bar.css"
import Axios from 'axios'
import PropTypes from 'prop-types';

export const SearchBar = ( {setResults,setSortBy,setSortOrder} ) => {
    const [input, setInput] = useState('');
    const [data, setData] = useState(() => {
        Axios.get('https://pokeapi.co/api/v2/pokemon?limit=1010&offset=0').then(
            (response) => {
                console.log(typeof response.data.results)
                 setData(response.data.results);
            } 
        )}
    );
    
    const handleChange = (value) => {
        setInput(value);
        setResults(search_pokemon(value));
    };

    handleChange.propTypes = { value: PropTypes.string } 

    const search_pokemon = (value) => {
        return value && data.filter(item => item.name.toLowerCase().includes(value.toLowerCase()));
    }

    return (
        <div className='search-box-wrapper'>
            <div className='input-wrapper' >
                <input className= 'search-input' placeholder='Type to Search' value={input} onChange={(e) => handleChange(e.target.value)}></input>
                <i className="fa-solid fa-magnifying-glass" id = 'searchIcon'></i>
            </div>
            <div className='list-order-wrapper'>
                <div className='list-order-element'>
                    <label htmlFor="sortBy" className='list-order-laber' >Sort By:</label>
                    <select onChange={(e) => setSortBy(e.target.value)} id='sortBy' >
                        <option value="ID">ID</option>
                        <option value="Name">Name</option>
                    </select>
                </div>
                <div className='list-order-element'>
                    <label htmlFor="Order"className='list-order-laber' >Order By:</label>
                    <select onChange={(e) => setSortOrder(e.target.value)} id='Order' >
                        <option value={'a'}>Ascending</option>
                        <option value={'d'}>Descending</option>
                    </select>
                </div>
            </div>
        </div>
    );
}