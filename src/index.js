import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { Route, Routes } from 'react-router-dom';
import { Gallery } from './Gallery/gallery';
import { Basic } from './Basic/basic';
import { List } from './List/list';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
        <BrowserRouter basename={process.env.PUBLIC_URL}>
        <div className='App' >
            <Routes>
                <Route  path={`/`} element={<Basic/>}> </Route>
                <Route  path={`list`} element={<List/>}></Route>
                <Route  path={`gallery`} element={<Gallery/>}></Route>
                <Route  path={`list/detail/:pokeId`} element={<List/>}></Route>
                <Route  path={`gallery/detail/:pokeId`} element={<Gallery/>}></Route>
            </Routes>
        </div>        
        </BrowserRouter>
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

