import React from 'react';
import { Basic } from '../Basic/basic';
import { ViewSwitch } from '../Basic/view-switch'; 
import { GalleryContainer } from './gallery-container';

export const Gallery = ( ) => {
    return (
        <div className='pokedex-wrapper'>
            <Basic />
            <div className='right-component'> 
                <div className='right-top'>
                    <div className='view-title'>Gallery View</div>
                    <ViewSwitch />
                </div>
                <div className="right-container">
                    <div className="right-container-black">
                        <div className="right-container-screen">
                            <GalleryContainer/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}