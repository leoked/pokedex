import React, {useState,useEffect} from 'react';
import Axios from 'axios';
import PropTypes from 'prop-types';

export const Picture = ({url}) => {
    Picture.propTypes = {
        url: PropTypes.string
    }
    const [pic,setPic] = useState('')

    useEffect(()=>{
        Axios.get(url).then(
            (response) => {
                setPic(response.data.sprites.front_default)
            } 
        )
    },[url])
    
    if(pic)
    return (
        <img className='gallery-pic' src={pic} alt=' ' ></img>
    )
}