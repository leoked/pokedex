import React, { useEffect, useState } from 'react';
import Axios from 'axios'
import './gallery-container.css'
import { Link } from 'react-router-dom'
import { Picture } from './picture';
import PropTypes from 'prop-types';

export const GalleryContainer = () => {
    const [offset, setOffset] = useState('0');
    const [limit, setLimit] = useState('1010');
    const [pokemon, setPokemon] = useState([]);
    const [curPage, setCurPage] = useState([]);
    const [pageNum, setPageNum] = useState(0); 

    useEffect(()=>{
        Axios.get(`https://pokeapi.co/api/v2/pokemon/?offset=${offset}&limit=${limit}`).then(
            (response) => {
                setPokemon(response.data.results)
                setCurPage(response.data.results.slice(0,12))
                setPageNum(0);
            } 
        )
    },[offset,limit])

    const handleChange = (offset, limit) => {
        setOffset(offset);
        setLimit(limit);
    }

    handleChange.propTypes = {
        offset: PropTypes.string,
        limit: PropTypes.string
    }

    const nextPage = (pokemon,pageNum,value) => {
        let n = pageNum + value;
        if(n >= 0 && n < pokemon.length)
        {
            if(n > n+value){
                setCurPage(pokemon.slice(pageNum+value,pageNum))
            }
            else{
                setCurPage(pokemon.slice(n,n+value))
            }
            setPageNum(n);
        }
        else if(pageNum > pokemon.length && n < pokemon.length)
        {
            setCurPage(pokemon.slice(n, pokemon.length-1))
            setPageNum(n);
        }
    }

    nextPage.propTypes = {
        pokemon: PropTypes.any,
        pageNum: PropTypes.number,
        value: PropTypes.number
    }

    // eslint-disable-next-line
    {
        return (
            <div className='gallery-container'>
                <div className='gallery-type-wrapper'>  
                    <div onClick={() => {handleChange('0','1010')}} className='gallery-type'> All </div>
                    <div onClick={() => {handleChange('0','151')}} className='gallery-type'> Gen 1 </div>
                    <div onClick={() => {handleChange('151','100')}} className='gallery-type'> Gen 2 </div>
                    <div onClick={() => {handleChange('251','135')}} className='gallery-type'> Gen 3 </div>
                    <div onClick={() => {handleChange('386','107')}} className='gallery-type'> Gen 4 </div>
                    <div onClick={() => {handleChange('493','156')}} className='gallery-type'> Gen 5 </div>
                    <div onClick={() => {handleChange('649','72')}} className='gallery-type'> Gen 6 </div>
                    <div onClick={() => {handleChange('721','88')}} className='gallery-type'> Gen 7 </div>
                    <div onClick={() => {handleChange('809','96')}} className='gallery-type'> Gen 8 </div>
                    <div onClick={() => {handleChange('905','105')}} className='gallery-type'> Gen 9 </div>
                </div>

                <div className='gallery-pic-wrapper'>
                {
                        curPage.map((pokemon,id)=>{
                            const urlArray = pokemon.url.split('/')
                            const pokeId = urlArray[6]
                            return (
                                <Link className='gallery-link' key={id} to = {`/gallery/detail/${pokeId}`}> 
                                    <Picture url={pokemon.url}/>
                                </Link>
                            )
                        })
                }
                </div>
                <div className='gallery-bar-wapper'>
                    <div onClick={()=>{nextPage(pokemon,pageNum,-12)}} className='gallerry-page-bar'> Prev</div>
                    <div onClick={()=>{nextPage(pokemon,pageNum,12)}} className='gallerry-page-bar'> Next</div>
                </div>
            </div>
        )
    }
    
}