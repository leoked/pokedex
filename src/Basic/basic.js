import React from 'react';
import './basic.css'
import { Detail } from '../Detail/detail';
import { Link, useParams } from 'react-router-dom'

export const Basic = () => {
    const {pokeId} = useParams()
    const getNext = (pokeId,value) =>{
        if(pokeId){
            let path = String(window.location.href.substring(0, window.location.href.lastIndexOf("/")) + '/')
            if(Number(pokeId)+value < 1){
                path += '1'
            }
            else if(Number(pokeId)+value > 1010)
            {
                path += '1010'
            }
            else{ path += `${String(Number(pokeId)+value)}`}
            return path
        }
        else{
            return window.location.href
        } 
    }

    return (
        <div className="pokedex">
            <div className="left-container">
                <div className="left-container__top-section">
                    <div className="top-section__blue"></div>
                    <div className="top-section__small-buttons">
                        <div className="top-section__red"></div>
                        <div className="top-section__yellow"></div>
                        <div className="top-section__green"></div>
                    </div>
                </div>

                <div className="left-container__main-section-container">
                    <div className="left-container__main-section">
                        <Detail />
                        <div className="left-container__controllers">
                            <div className="controllers__d-pad">
                                <div className="d-pad__cell top"></div>
                                <Link to={getNext(pokeId,-1)} className="d-pad__cell left"></Link>
                                <div className="d-pad__cell middle"></div>
                                <Link to={getNext(pokeId,1)} className="d-pad__cell right"></Link>
                                <div className="d-pad__cell bottom"></div>
                            </div>

                            <div className="controllers__buttons">
                                <div className="buttons__button">B</div>
                                <div className="buttons__button">A</div>
                            </div>
                        </div>
                    </div>
                    
                    <div className="left-container__right">
                        <div className="left-container__hinge"></div>
                        <div className="left-container__hinge"></div>
                    </div>
                </div>
            </div>
        </div>
    )
}