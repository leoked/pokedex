import React from 'react';
import { NavLink } from 'react-router-dom';
import './view-switch.css'

export const ViewSwitch = () => {
    return (
        <div className='view-switcher' >
            <NavLink to={'/list'}> <i className="fa-solid fa-list view-changer" id='list-view'></i> </NavLink>
            <NavLink to={'/gallery'}> <i className="fa-solid fa-border-all view-changer" id='gallery-view'></i> </NavLink>
        </div>
    )
}