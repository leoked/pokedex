import React, {useState,useEffect} from 'react';
import "./detail.css"
import Axios from 'axios'
import { useParams, NavLink } from 'react-router-dom';

const color = {'fire'    : { 'prime': '#F08030','second': '#f5ac78'},
               'grass'   : { 'prime': '#78C850','second': '#a7db8d'},
               'water'   : { 'prime': '#6890F0','second': '#9db7f5'},
               'bug'     : { 'prime': '#A8B820','second': '#c6d16e'},
               'normal'  : { 'prime': '#A8A878','second': '#c6c6a7'},
               'poison'  : { 'prime': '#A040A0','second': '#c183c1'},
               'electric': { 'prime': '#F8D030','second': '#fae078'},
               'ground'  : { 'prime': '#E0C068','second': '#ebd69d'},
               'fairy'   : { 'prime': '#EE99AC','second': '#f4bdc9'},
               'fighting': { 'prime': '#C03028','second': '#d67873'},
               'psychic' : { 'prime': '#F85888','second': '#fa92b2'},
               'rock'    : { 'prime': '#B8A038','second': '#d1c17d'},
               'ghost'   : { 'prime': '#705898','second': '#a292bc'},
               'ice'     : { 'prime': '#98D8D8','second': '#bce6e6'},
               'dragon'  : { 'prime': '#7038F8','second': '#a27dfa'},
               'dark'    : { 'prime': '#705848','second': '#a29288'},
               'steel'   : { 'prime': '#B8B8D0','second': '#d1d1e0'},
               'flying'  : { 'prime': '#A890F0','second': '#c6b7f5'}}

export const Detail = () => {
    const {pokeId} = useParams()
    const [pokemon, setPokmon] = useState([]);
    const [notload,setNotload] = useState(false);

    useEffect(() =>{
        if(pokeId && Number(pokeId) > 0 && Number(pokeId) <= 1010){
            Axios.get(`https://pokeapi.co/api/v2/pokemon/${pokeId}`).then(
                (response) => {
                     setPokmon(response.data);
                     setNotload((pre)=> pre = false);
                } 
            )
        }
        else{
            setNotload((pre)=> pre = true);
        }
    },[pokeId])
    
    if(notload){
        return(
        <div className='loading'>
            <div className='loading-screen'>
                <NavLink className='loading-screen-element' to={'/list'}> List View </NavLink>
                <NavLink className='loading-screen-element' to={'/gallery'}> Gallery View </NavLink>
            </div>
        </div>
        ) 
    }

    if(pokemon.id){
        const backgroud_type = pokemon.types[0].type.name;
        return (
            <div className='detail-view' style={{backgroundColor : color[backgroud_type]['prime']}}>
                <div className='detail-colmn-1'>
                    <div className='poke-name'> <h1>{pokemon.name.charAt(0).toUpperCase() + pokemon.name.slice(1)}</h1> </div>
                    <div className='poke-id'> <h1>#{pokemon.id} </h1></div>
                </div>

                <div className='detail-colmn-2'>
                    <img className='poke-image' src={pokemon.sprites['front_default']} alt="" />
            
                    <div className='poke-info'>
                        <div className='poke-info-type info-header' style={{backgroundColor : color[backgroud_type]['second']}}> <h2>Type</h2> 
                            <div className='info-content type-content'>
                            { 
                                pokemon.types.map(({type}) => 
                                (
                                    <div className='type-content-text' key={type.name}>{type.name.charAt(0).toUpperCase() + type.name.slice(1)}</div>
                                ))
                            }
                            </div>
                        </div>
                        <div className='poke-info-hw-wrapper info-header'>
                            <div className='poke-info-height' style={{backgroundColor : color[backgroud_type]['second']}}> <h2>Height</h2> <div className='info-content'>{pokemon.height/10} m</div> </div>
                            <div className='poke-info-weight' style={{backgroundColor : color[backgroud_type]['second']}}> <h2>Weight</h2> <div className='info-content'>{pokemon.weight/10} kg</div></div>
                        </div>
                        <div className='poke-info-abilities info-header' style={{backgroundColor : color[backgroud_type]['second']}}>
                            <h2>Abilities</h2>
                            <div className='info-content'>
                            { 
                                pokemon.abilities.map(({ability},id) => 
                                (
                                    <div key={id}>{ability.name.charAt(0).toUpperCase() + ability.name.slice(1)}</div>
                                ))
                            }
                            </div>
            
                        </div>
                    </div>
                </div>

                <div className='detail-colmn-3 info-header'style={{backgroundColor : color[backgroud_type]['second']}}>
                    <h2>Stat</h2>
                    <div className='stat-wapper info-content'>
                    { 
                        pokemon.stats.map(({base_stat,stat},index) => 
                        (
                        <span className={'stats-bar stats-bar-'+index} key={stat.name}> {stat.name.padEnd(23,".") + base_stat} </span>
                        ))
                    }
                    </div>
                </div>
            </div>
        )  
    }
}